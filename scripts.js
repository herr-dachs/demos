var source,
    player,
    playerContainer = document.getElementById('player-container'),
    conf = {
        key: 'd94cfbfe-829f-4f35-8f98-8b0b1a784647',
        playback: {
            autoplay: true,
            muted: false,
            subtitleLanguage: 'de'
        },
        style: {
            aspectratio: '16/9'
        },
        events: {
            [bitmovin.player.PlayerEvent.VolumeChanged]: (event) => {
                console.log('VOLUME CHANGED TO:', event.targetVolume);
            },
            [bitmovin.player.PlayerEvent.Muted]: () => {
                console.log('MUTED');
            }
        }
    };

source = {
    dash: 'https://bitmovin-a.akamaihd.net/content/sintel/sintel.mpd',
};

player = new bitmovin.player.Player(playerContainer, conf);
player.load(source);
